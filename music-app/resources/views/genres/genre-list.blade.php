@foreach ($genres as $key => $genre)
    <tr>
        <td>{{ $key + 1 }}</td>
        <td>{{ $genre->name }}</td>
        <td>
            <a href='#' class='btn btn-success edit' data-id='{{ $genre->id }}' data-name='{{ $genre->name }}'>
                Edit</a>
            <a href='#' class='btn btn-danger delete' data-id='{{ $genre->id }}'> Delete</a>
        </td>
    </tr>
@endforeach

