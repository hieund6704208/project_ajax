@extends('genres.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Danh sách thể loại</h5>
                    <div class="pull-right mb-2">
                        {{-- <a class="btn btn-success" href="{{ route('genre.create') }}"> Create Genre</a> --}}
                        <button type="button" id="add" data-bs-toggle="modal" data-bs-target="#addnew"
                            class="btn btn-primary pull-right"> Create Genre</button>
                    </div>
                    <div class="pull-left mb-2">
                        {{-- <a class="btn btn-success" href="{{ route('genre.create') }}"> Create Genre</a> --}}
                        <input type="text" class="form-controller" id="search" name="search">
                    </div>

                    <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Thể loại</th>
                                    <th>Acion</th>
                                </tr>
                            </thead>
                            <tbody id="genreBody">

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Thể loại</th>
                                    <th>Acion</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="pagination">

    </div>
@endsection

@section('script')
    <script type="text/javascript">
        function showGenre(page) {
            $.get("{{ route('show-genre') }}", {
                page: page
            }, function(data) {
                $('#genreBody').empty().html(data.html);
                $('#pagination').empty().html(data.pagination);
            })
        }
        $(document).off('click').on('click', '.pagination a', function(e) {
            e.preventDefault();
            var page = $(this).attr('href').split('page=')[1];
            showGenre(page);
        });

        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            showGenre();

            $('#search').on('keyup',function(){
                let search = $(this).val();
                $.ajax({
                    type: 'get',
                    url: "{{ route('search') }}",
                    data: {
                        search: search,
                    },
                    dataType: 'html',
                    success:function(response){
                        $('#genreBody').empty().append(response);
                    }
                });
            })
            search();

            function search() {
                $("#search").on("change", function(e) {
                    e.preventDefault();
                    var url = "/genre/search";
                    var data = {
                        name: $("#search").val(),
                    };
                    console.log(data);
                    console.log(data, url);
                    $.ajax({
                        type: "GET",
                        url: url,
                        data: data,
                        success: function(data) {
                            console.log(data);
                            $("#genreBody").html(data);
                        },
                        error: function(data) {
                            console.log(data.responseJSON);

                        },
                        done: function(data) {
                            console.log(data);
                        },
                    });
                });
            }

            $('#addForm').on('submit', function(e) {
                e.preventDefault();
                var form = $(this).serialize();
                console.log(form);
                var url = $(this).attr('action');
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: form,
                    dataType: 'json',
                    success: function() {
                        $('#addnew').modal('hide');
                        $('#addForm')[0].reset();
                        showGenre();
                    },
                    error: function(result) {
                        console.log(result.responseJSON);
                        console.log(result.responseJSON.message);
                        Swal.fire({
                            icon: "error",
                            title: "Oops...",
                            text: result.responseJSON.message,
                            footer: '<a href="">Why do I have this issue?</a>',
                        });
                    },
                });
            });

            $(document).on('click', '.edit', function(event) {
                event.preventDefault();
                var id = $(this).data('id');
                var name = $(this).data('name');
                $('#editmodal').modal('show');
                $('#name').val(name);
                $('#genreid').val(id);
            });

            $('#editForm').on('submit', function(e) {
                e.preventDefault();
                var form = $(this).serialize();
                var id = $('#genreid').val();
                $.ajax({
                    type: 'PUT',
                    url: "{{ route('genre.update', ['genre' => ':id']) }}".replace(':id', id),
                    data: form,
                    success: function() {
                        $('#editmodal').modal('hide');
                        showGenre();
                    },
                    error: function(result) {
                        console.log(result.responseJSON);
                        console.log(result.responseJSON.message);
                        Swal.fire({
                            icon: "error",
                            title: "Oops...",
                            text: result.responseJSON.message,
                            footer: '<a href="">Why do I have this issue?</a>',
                        });
                    },
                });
            });

            $(document).on('click', '.delete', function(event) {
                event.preventDefault();
                var id = $(this).data('id');
                $('#deletemodal').modal('show');
                $('#deletemember').val(id);
            });


            $('#deletemember').click(function() {
                var id = $(this).val();
                console.log(id);
                $.ajax({
                    type: 'DELETE',
                    url: "{{ route('genre.destroy', ['genre' => ':id']) }}".replace(':id', id),
                    success: function() {
                        $('#deletemodal').modal('hide');
                        showGenre();
                    },
                    error: function() {}
                });
            });


        });
    </script>
@endsection
