@extends('songs.app')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Danh sách bài hát</h5>
                    <div class="pull-right mb-2">
                        {{-- <a class="btn btn-success" href="{{ route('song.create') }}"> Create Song</a> --}}
                        <button type="button" id="add" data-bs-toggle="modal" data-bs-target="#addnew"
                            class="btn btn-primary pull-right"> Create Song</button>
                    </div>
                    <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                <tr>
                                    <th>#</th>
                                    <th>Tên bài hát</th>
                                    <th>Tên ca sĩ</th>
                                    <th>Thể loại</th>
                                    <th>Album</th>
                                    <th>Bài hát</th>
                                    <th>Giá</th>
                                    <th>Acion</th>
                                </tr>
                                </tr>
                            </thead>
                            <tbody id="songBody">

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Tên bài hát</th>
                                    <th>Tên ca sĩ</th>
                                    <th>Thể loại</th>
                                    <th>Album</th>
                                    <th>Bài hát</th>
                                    <th>Giá</th>
                                    <th>Acion</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        function showSong() {
            $.get("{{ route('show-song') }}", function(data) {
                $('#songBody').empty().html(data);
            })
        }

        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            showSong();

            $.get("{{ route('call-list-song') }}", function(data) {
                data.albums.data.forEach((element, index) => {
                    $('#album_id').append("<option value=" + element.id + ">" + element.name +
                        "</option>");
                });
                data.albums.data.forEach((element, index) => {
                    $('#album_id_edit').append(
                        `<option value = "${element.id}" > ${element.name} </option>`);
                });
            })

            $('#addForm').on('submit', function(e) {
                e.preventDefault();
                var form = $(this).serialize();
                var url = $(this).attr('action');
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: new FormData(this),
                    contentType: false,
                    processData: false,
                    success: function() {
                        $('#addnew').modal('hide');
                        $('#addForm')[0].reset();
                        showSong();
                    },
                    error: function(result) {
                        console.log(result.responseJSON);
                        console.log(result.responseJSON.message);
                        Swal.fire({
                            icon: "error",
                            title: "Oops...",
                            text: result.responseJSON.message,
                            footer: '<a href="">Why do I have this issue?</a>',
                        });
                    },
                });
            });

            $(document).on('click', '.edit', function(event) {
                event.preventDefault();
                var id = $(this).data('id');
                $('#albumid').val(id);
                var name = $(this).data('name');
                var album_id = $(this).data('album_id');
                var image = $(this).data('file');
                console.log(image);
                var price = $(this).data('price');
                $('#editmodal').modal('show');
                $('#name').val(name);
                $('#album_id').val(album_id);
                $('#image').val(image);
                $('#price').val(price);
            });

            $('#editForm').on('submit', function(e) {
                e.preventDefault();
                var form = $(this).serialize();
                var id = $('#albumid').val();

                $.ajax({
                    type: 'POST',
                    url: "{{ route('song.update', ['song' => ':id']) }}".replace(':id', id),
                    data: new FormData(this),
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    success: function() {
                        $('#editmodal').modal('hide');
                        showSong();
                    },
                    error: function(result) {
                        console.log(result.responseJSON);
                        console.log(result.responseJSON.message);
                        Swal.fire({
                            icon: "error",
                            title: "Oops...",
                            text: result.responseJSON.message,
                            footer: '<a href="">Why do I have this issue?</a>',
                        });
                    },
                });
            });

            $(document).on('click', '.delete', function(event) {
                event.preventDefault();
                var id = $(this).data('id');
                $('#deletemodal').modal('show');
                $('#deletemember').val(id);
            });

            $('#deletemember').click(function() {
                var id = $(this).val();
                $.ajax({
                    type: 'DELETE',
                    url: "{{ route('song.destroy', ['song' => ':id']) }}".replace(':id', id),
                    success: function() {
                        $('#deletemodal').modal('hide');
                        showSong();
                    },
                    error: function() {}
                });
            });
        });
    </script>
@endsection
