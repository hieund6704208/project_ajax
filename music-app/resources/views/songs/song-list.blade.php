@foreach ($songs as $key => $song)
    <tr>
        <td>{{ $key + 1 }}</td>
        <td>{{ $song->name }}</td>
        <td>{{ $song->album->singer->name }}</td>
        <td>{{ $song->album->genre->name }}</td>
        <td>{{ $song->album->name }}</td>
        <td>
            <audio controls>
                <source src="{{ asset('singers/' . $song->file) }}" type="audio/mpeg">
            </audio>
        </td>
        <td>{{ $song->price }}</td>
        <td>
            <a href='#' class='btn btn-success edit' data-id='{{ $song->id }}' data-name='{{ $song->name }}'
                data-album='{{ $song->album_id }}' data-price='{{ $song->price }}'>
                Edit</a>
            <a href='#' class='btn btn-danger delete' data-id='{{ $song->id }}'> Delete</a>
        </td>
    </tr>
@endforeach
