@extends('albums.app')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Danh sách album</h5>
                    <div class="pull-right mb-2">
                        {{-- <a class="btn btn-success" href="{{ route('album.create') }}"> Create Album</a> --}}
                        <button type="button" id="add" data-bs-toggle="modal" data-bs-target="#addnew"
                            class="btn btn-primary pull-right"> Create Album</button>
                    </div>
                    <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Tên album</th>
                                    <th>Ca sĩ</th>
                                    <th>Thể loại</th>
                                    <th>Mô tả</th>
                                    <th>Acion</th>
                                </tr>
                            </thead>
                            <tbody id="albumBody">

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Tên album</th>
                                    <th>Ca sĩ</th>
                                    <th>Thể loại</th>
                                    <th>Mô tả</th>
                                    <th>Acion</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    </div>
    <div id="pagination">

    </div>
@endsection

@section('script')
    <script type="text/javascript">
        function showAlbum(page) {
            $.get("{{ route('show-album') }}", {
                page: page
            }, function(data) {
                $('#albumBody').empty().html(data.html);
                $('#pagination').html(data.pagination);
            })
        }
        $(document).off('click').on('click', '.pagination a', function(e) {
            e.preventDefault();
            var page = $(this).attr('href').split('page=')[1];
            showAlbum(page);
        });

        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            showAlbum();

            $.get("{{ route('call-list') }}", function(data) {

                data.genres.data.forEach((element, index) => {
                    $('#genre_id').append("<option value=" + element.id + ">" + element.name + "</option>");
                });
                data.singers.data.forEach((element, index) => {
                    $('#artist_id').append("<option value=" + element.id + ">" + element.name + "</option>");
                    });
                    data.genres.data.forEach((element, index) => {
                        $('#genre_id_edit').append(`<option value = "${element.id}" > ${element.name} </option>`);

                    });
                    data.singers.data.forEach((element, index) => {
                        $('#artist_id_edit').append(`<option value = "${element.id}" > ${element.name} </option>`);
                    });
            })

            $('#addForm').on('submit', function(e) {
                e.preventDefault();
                var form = $(this).serialize();
                var url = $(this).attr('action');
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: form,
                    dataType: 'json',
                    success: function() {
                        $('#addnew').modal('hide');
                        $('#addForm')[0].reset();
                        showAlbum();
                    },
                    error: function(result) {
                        console.log(result.responseJSON);
                        console.log(result.responseJSON.message);
                        Swal.fire({
                            icon: "error",
                            title: "Oops...",
                            text: result.responseJSON.message,
                            footer: '<a href="">Why do I have this issue?</a>',
                        });
                    },
                });
            });

            $(document).on('click', '.edit', function(event) {

                event.preventDefault();
                var id = $(this).data('id');
                $('#albumid').val(id);
                var name = $(this).data('name');
                var genre_id = $(this).data('genre');
                var artist_id = $(this).data('singer');
                var description = $(this).data('description');
                $('#editmodal').modal('show');
                $('#name').val(name);
                $('#genre_id').val(genre_id);
                $('#artist_id').val(artist_id);
                $('#description').val(description);
            });

            $('#editForm').on('submit', function(e) {
                console.log(123);
                e.preventDefault();
                var form = $(this).serialize();
                var id = $('#albumid').val();

                $.ajax({
                    type: 'POST',
                    url: "{{ route('album.update', ['album' => ':id']) }}".replace(':id', id),
                    data: form,
                    success: function() {
                        $('#editmodal').modal('hide');
                        showAlbum();
                    },
                    error: function(result) {
                        console.log(result.responseJSON);
                        console.log(result.responseJSON.message);
                        Swal.fire({
                            icon: "error",
                            title: "Oops...",
                            text: result.responseJSON.message,
                            footer: '<a href="">Why do I have this issue?</a>',
                        });
                    },
                });
            });

            $(document).on('click', '.delete', function(event) {
                event.preventDefault();
                var id = $(this).data('id');
                $('#deletemodal').modal('show');
                $('#deletemember').val(id);
            });

            $('#deletemember').click(function() {
                var id = $(this).val();
                $.ajax({
                    type: 'DELETE',
                    url: "{{ route('album.destroy', ['album' => ':id']) }}".replace(':id', id),
                    success: function() {
                        $('#deletemodal').modal('hide');
                        showAlbum();
                    },
                    error: function() {}
                });
            });
        });
    </script>
@endsection
