@foreach ($albums as $key => $album)
<tr>
    <td>{{ $key + 1 }}</td>
    <td>{{ $album->name }}</td>
    <td>{{ $album->singer->name }}</td>
    <td>{{ $album->genre->name }}</td>
    <td>{{ $album->description }}</td>
    <td>
        <a href='#' class='btn btn-success edit' data-id='{{ $album->id }}' data-name='{{ $album->name }}' data-genre='{{ $album->genre_id }}' data-singer='{{ $album->artist_id }}'  data-description='{{ $album->description }}' >
            Edit</a>
        <a href='#' class='btn btn-danger delete' data-id='{{ $album->id }}'> Delete</a>
    </td>
</tr>
@endforeach
