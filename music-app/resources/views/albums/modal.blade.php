<!-- Add Modal -->
<div class="modal fade" id="addnew" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel">Thêm Album</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{ route('album.store') }}" id="addForm" enctype="multipart/form-data">
                    <div class="mb-3">
                        <label for="firstname">Album</label>
                        <input type="text" name="name" class="form-control" placeholder="Tên Album">
                    </div>
                    <div class="mb-3">
                        <label for="">Thể loại:</label>
                        <div>
                            <select name="genre_id" id="genre_id" class="select2 form-select shadow-none" required>
                                <option selected>Choose...</option>
                            </select>
                        </div>
                    </div>
                    <div class="mb-3">
                        <label>Ca sĩ</label>
                        <div>
                            <select name="artist_id" id="artist_id" class="select2 form-select shadow-none">
                                <option selected>Choose...</option>
                            </select>
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="firstname">Mô tả</label>
                        <input type="text" class="form-control" placeholder="Mô tả" name="description">
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Edit Modal -->
<div class="modal fade" id="editmodal" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel">Sửa album</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="editForm" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <input type="hidden" id="albumid" name="id">
                    <div class="mb-3">
                        <label for="">Tên album</label>
                        <input type="text" name="name" id="name" class="form-control">
                    </div>
                    <div class="mb-3">
                        <label for="">Thể loại:</label>
                        <div>
                            <select name="genre_id" id="genre_id_edit" class="select2 form-select shadow-none">
                            </select>
                        </div>
                    </div>
                    <div class="mb-3">
                        <label>Ca sĩ</label>
                        <div>
                            <select name="artist_id" id="artist_id_edit" class="select2 form-select shadow-none">
                            </select>
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="">Mô tả</label>
                        <input type="text" class="form-control" id="description" name="description">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Delete Modal -->
<div class="modal fade" id="deletemodal" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel">Delete Member</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <h4 class="text-center">Are you sure you want to delete Member?</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" id="deletemember" class="btn btn-danger">Delete</button>
            </div>
        </div>
    </div>
</div>
