@foreach ($singers as $key => $singer)
    <tr>
        <td>{{ $key + 1 }}</td>
        <td>{{ $singer->name }}</td>
        <td>
            <img style="width: 100px; height: 100px;" src="{{ asset('singers') }}/{{ $singer->image }}" alt="">
        </td>
        <td>
            <a href='#' class='btn btn-success edit' data-id='{{ $singer->id }}' data-name='{{ $singer->name }}'
                data-image={{ $singer->image }}>
                Edit</a>
            <a href='#' class='btn btn-danger delete' data-id='{{ $singer->id }}'> Delete</a>
        </td>
    </tr>
@endforeach
