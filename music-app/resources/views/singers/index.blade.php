@extends('singers.app')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Danh sách ca sĩ</h5>
                    <div class="pull-right mb-2">
                        {{-- <a class="btn btn-success" href="{{ route('singer.create') }}"> Create Singer</a> --}}
                        <button type="button" id="add" data-bs-toggle="modal" data-bs-target="#addnew"
                            class="btn btn-primary pull-right"> Create Singer</button>
                    </div>
                    <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                <tr>
                                    <th>#</th>
                                    <th>Tên ca sĩ</th>
                                    <th>Hình ảnh</th>
                                    <th>Acion</th>
                                </tr>
                                </tr>
                            </thead>
                            <tbody id="singerBody">

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Tên ca sĩ</th>
                                    <th>Hình ảnh</th>
                                    <th>Acion</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        function showSinger() {
            $.get("{{ route('show-singer') }}", function(data) {
                $('#singerBody').empty().html(data);
            })
        }

        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            showSinger();

            $('#addForm').on('submit', function(e) {
                e.preventDefault();
                var form = $(this).serialize();

                var url = $(this).attr('action');
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: new FormData(this),
                    contentType: false,
                    processData: false,
                    dataType: 'json',
                    success: function() {
                        $('#addnew').modal('hide');
                        $('#addForm')[0].reset();
                        showSinger();
                    },
                    error: function(result) {
                        console.log(result.responseJSON);
                        console.log(result.responseJSON.message);
                        Swal.fire({
                            icon: "error",
                            title: "Oops...",
                            text: result.responseJSON.message,
                            footer: '<a href="">Why do I have this issue?</a>',
                        });
                    },
                });
            });

            $(document).on('click', '.edit', function(event) {
                event.preventDefault();
                var id = $(this).data('id');
                $('#singerid').val(id);
                console.log(this);
                var name = $(this).data('name');
                var image = $(this).data('image');
                $('#editmodal').modal('show');
                $('#name').val(name);
                $('#image').val(image);
            });

            $('#editForm').on('submit', function(e) {
                e.preventDefault();
                let id = $('#singerid').val();
                let name = $('#name').val();
                let image = $('#image').val();
                console.log(id, name, image);
                $.ajax({
                    type: 'post',
                    url: "{{ route('singer.update', ['singer' => ':id']) }}".replace(':id', id),
                    // data: {
                    //     name : name,
                    //     image :image,
                    //     "_token": "{{ csrf_token() }}",
                    // },
                    data: new FormData(this),
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    success: function() {
                        $('#editmodal').modal('hide');
                        showSinger();
                    },
                    error: function(result) {
                        console.log(result.responseJSON);
                        console.log(result.responseJSON.message);
                        Swal.fire({
                            icon: "error",
                            title: "Oops...",
                            text: result.responseJSON.message,
                            footer: '<a href="">Why do I have this issue?</a>',
                        });
                    },
                });
            });

            $(document).on('click', '.delete', function(event) {
                event.preventDefault();
                var id = $(this).data('id');
                $('#deletemodal').modal('show');
                $('#deletemember').val(id);
            });


            $('#deletemember').click(function() {
                var id = $(this).val();
                $.ajax({
                    type: 'DELETE',
                    url: "{{ route('singer.destroy', ['singer' => ':id']) }}".replace(':id', id),
                    success: function() {
                        $('#deletemodal').modal('hide');
                        showSinger();
                    },
                    error: function() {}
                });
            });
        });
    </script>
@endsection
