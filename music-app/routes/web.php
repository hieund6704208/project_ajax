<?php

use App\Http\Controllers\AlbumController;
use App\Http\Controllers\Customer\CoinController;
use App\Http\Controllers\Customer\CustomerController;
use App\Http\Controllers\Customer\DownloadController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\SingerController;
use App\Http\Controllers\SongController;
use App\Http\Controllers\Customer\SongController as SonggController;
use App\Http\Controllers\FavoriteController;
use App\Http\Controllers\UserController;
use App\Http\Middleware\CheckRole;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'music' ], function()
{
    Route::get('login',[LoginController::class,'showLoginForm'])->name('login');
    Route::post('login',[LoginController::class,'login']);
    Route::get('register',[LoginController::class,'showRegistrationForm'])->name('register');
    Route::post('register',[LoginController::class,'register']);
    Route::get('logout',[LoginController::class,'logout'])->name('logout');
});

Route::group(['middleware' => 'checkrole'],function () {
    Route::resource('/genre',GenreController::class);
    Route::get('list', [GenreController::class, 'getMembers'])->name('show-genre');
    Route::get('search', [GenreController::class, 'search'])->name('search');
    Route::resource('/singer',SingerController::class);
    Route::get('list-singer', [SingerController::class, 'getSingers'])->name('show-singer');
    Route::resource('/album',AlbumController::class);
    Route::get('list-album', [AlbumController::class, 'getAlbums'])->name('show-album');
    Route::get('call-album', [AlbumController::class, 'callOption'])->name('call-list');
    Route::resource('/song',SongController::class);
    Route::get('list-song', [SongController::class, 'getSongs'])->name('show-song');
    Route::get('call-song', [SongController::class, 'callOption'])->name('call-list-song');
});

Route::post('/favorite/add/{song}',[ FavoriteController::class,'addFavorite'])->name('add');
Route::delete('/favorite/del/{song}',[ FavoriteController::class,'removeFavorite'])->name('del');
Route::get('/favorite/index',[ FavoriteController::class,'index'])->name('index');
Route::resource('/customer',SonggController::class);
Route::get('/home',[CustomerController::class, 'index'])->name('home');
Route::resource('/customer/coin',CoinController::class);
Route::resource('/user', UserController::class);
Route::resource('/coin',CoinController::class);
Route::post('/download', [DownloadController::class,'download'])->name('download');
Route::post('/user/addcoin/{id}', [UserController::class,'deposit'])->name('deposit');

