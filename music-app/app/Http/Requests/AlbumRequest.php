<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AlbumRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'genre_id' => 'required',
            'artist_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Không được bỏ trống tên',
            'genre_id.required' => 'Không được bỏ thể loại',
            'artist_id.required' => 'Không được bỏ ca sĩ',
        ];
    }
}
