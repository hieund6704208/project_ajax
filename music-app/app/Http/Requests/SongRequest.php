<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SongRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'image' => 'required|mimetypes:audio/mpeg|max:50000',
            'price'=> 'min:0|regex:/^[0-9]+$/'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Không được bỏ trống tên',
            'image.required' => 'Không được bỏ trống file MP3',
            'image.mimetypes' => 'Không đúng định dạng file audio/mpeg',
            'price.min' => 'Không được nhỏ hơn 0',
            'price.regex' => 'Không được là chữ',
        ];
    }
}
