<?php

namespace App\Http\Controllers;

use App\Http\Requests\AlbumRequest;
use App\Services\AlbumService;
use App\Services\GenreService;
use App\Services\SingerService;
use Exception;
use Illuminate\Http\Request;

class AlbumController extends Controller
{
    protected $albumService;
    protected $genreService;
    protected $singerService;

    public function __construct(AlbumService $albumService, GenreService $genreService, SingerService $singerService)
    {
        $this->middleware('auth:web', ['except' => 'login']);
        $this->albumService = $albumService;
        $this->genreService = $genreService;
        $this->singerService = $singerService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('albums.index');
    }

    public function getAlbums(Request $request){
        if($request->ajax()) {
            $albums = $this->albumService->getAllAlbums();
            $pagination = $albums->links()->render();
            $view = view('albums.album-list', compact('albums'))->render();
            return response()->json(['html' => $view, 'pagination' => $pagination]);
        }
        return view('albums.index');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // try {
        //     $genres = $this->genreService->getAllGenres();
        //     $singers = $this->singerService->getAll();
        //     return view('albums.create', compact('genres', 'singers'));
        // } catch (Exception $exception) {
        //     return back()->withError($exception->getMessage());
        // }
    }

    public function callOption()
    {
        $data =[
            "genres"=>$this->genreService->getAllGenres(),
            "singers"=>$this->singerService->getAll(),
        ];
        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AlbumRequest $request)
    {
        try {
           $album = $this->albumService->createAlbum($request->all());
            return response($album);
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // try {
        //     $genres = $this->genreService->getAllGenres();
        //     $singers = $this->singerService->getAll();
        //     $album = $this->albumService->getAlbumById($id);
        //     return view('albums.edit', compact('album', 'genres', 'singers'));
        // } catch (Exception $exception) {
        //     return back()->withError($exception->getMessage());
        // }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AlbumRequest $request, $id)
    {
        try {
            $data = $request->all();
            // dd($data);
            $album = $this->albumService->updateAlbum($id, $data);
            return response($album);
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($albumId)
    {
        try {
            $this->albumService->deleteAlbum($albumId);
            return response()->json(200);
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage());
        }
    }
}
