<?php

namespace App\Http\Controllers;

use App\Http\Requests\GenreRequest;
use App\Models\Genre;
use App\Services\GenreService;
use Exception;
// use App\Repositories\GenreRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;

class GenreController extends Controller
{
    protected $genreService;

    public function __construct(GenreService $genreService)
    {
        $this->middleware('auth:web', ['except' => 'login']);
        $this->genreService = $genreService;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('genres.genre');
    }
    public function getMembers(Request $request){
        if($request->ajax()) {
            $genres = $this->genreService->getAllGenres();
            $pagination = $genres->links()->render();
            $view =  view('genres.genre-list', compact('genres'))->render();
            return response()->json(['html' => $view, 'pagination' => $pagination]);
        }
        return view('genres.genre');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            return view('genres.create');
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage());
        }
    }

    // public function search(Request $request)
    // {
    //     dd($request);
    //         $genres = DB::table('genres')->where('name', 'LIKE', '%' . request()->all() . '%')->get();

    //         return view('genres.genre_list');
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GenreRequest $request)
    {
        try {
            $genreData = $request->all();
            $genre = $this->genreService->createGenre($genreData);
            return response($genre);
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($genreId)
    {
        // try {
        //     $category = $this->genreService->getGenreById($genreId);
        //     return view('genres.update', compact('category'));
        // } catch (Exception $exception) {
        //     return back()->withError($exception->getMessage());
        // }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(GenreRequest $request, $genreId)
    {
        try {
            $categoryData = $request->validated();
            Log::alert($categoryData);
            $category = $this->genreService->updateGenre($genreId, $categoryData);
            return response()->json(200);
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($genreId)
    {
        try {
            $genres = $this->genreService->deleteGenre($genreId);
            return response()->json(200);
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage());
        }
    }
}
