<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    use HasFactory;

    protected $table ='genres';

    protected $fillable =['name'];

    // public function scopeSearch($query){
    //     $query = Genre::query();
    //     if (!empty(request()->name)) {
    //         $query->where('name', 'like', '%' . request()->name . '%');
    //     }
    //     return $query;
    // }
}
