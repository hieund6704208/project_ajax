<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    use HasFactory;

    protected $fillable =[
        'name',
        'artist_id',
        'genre_id',
        'description'
    ];

    // public function user()
    // {
    //     return $this->belongsTo(User::class);
    // }

    public function singer()
    {
        return $this->belongsTo(Singer::class,'artist_id', 'id');
    }

    public function genre()
    {
        return $this->belongsTo(Genre::class, 'genre_id', 'id');
    }

    // public function songs()
    // {
    //     return $this->hasMany(Song::class);
    // }
}
