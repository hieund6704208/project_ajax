<?php

namespace App\Services;

use App\Models\Album;

    class AlbumService{
        public function getAllAlbums()
        {
            return Album::paginate(INDEX_ALBUMS);
        }

        public function getAlbumById($albumId)
        {
            return Album::findOrFail($albumId);
        }

        public function createAlbum($albumData)
        {
            $album = Album::create($albumData);
             return $album;
        }

        public function updateAlbum($albumId, $AlbumData)
        {
            $album = Album::findOrFail($albumId);
            $album->update($AlbumData);
            return $album;
        }

        public function deleteAlbum($albumId)
        {
            $album = Album::findOrFail($albumId);
            $album->delete();
        }

    }
