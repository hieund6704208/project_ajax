<?php

namespace App\Services;

use App\Models\Genre;

    class GenreService{
        public function getAllGenres()
        {
            return Genre::orderByDesc('id')->paginate(INDEX_GENRES);
        }

        public function getGenreById($genreId)
        {
            return Genre::findOrFail($genreId);
        }

        public function createGenre($genreData)
        {

                $genre = new Genre;
                $genre->name = $genreData['name'];
                $genre->save();
                return $genre;
        }

        public function updateGenre($genreId, $GenreData)
        {
            $category = Genre::findOrFail($genreId);
            $category->update($GenreData);
            return $category;
        }

        public function deleteGenre($genreId)
        {
            $category = Genre::findOrFail($genreId);
            $category->delete();
        }

    }
