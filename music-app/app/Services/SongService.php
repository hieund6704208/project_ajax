<?php

namespace App\Services;
use Illuminate\Support\Facades\File;


use App\Models\Song;

    class SongService{
        public function getAllSongs()
        {
            return Song::paginate(INDEX_SONGS);
        }

        public function getSongById($songId)
        {
            return Song::findOrFail($songId);
        }

        public function createSong($songData)
        {
        $name = $songData['name'];
        $album_id = $songData['album_id'];
        $price = $songData['price'];

        if ($songData['file']) {
            $image = $songData['file'];
            $fileName = time() . '.'. $image->getClientOriginalExtension();
            $image->move(public_path('singers'),$fileName);
            $song = new Song();
            $song->name = $name;
            $song->album_id = $album_id;
            $song->price = $price;
            $song->file = $fileName;
            $song->save();
        }
        // return $song;
        }

        public function updateSong($songId, $songData)
        {
        $song = Song::find($songId);
        $name = $songData['name'];
        $album_id = $songData['album_id'];
        $price = $songData['price'];

            if ($songData['file']) {
                $image = $songData['file'];
                $fileName = time() . '.'. $image->getClientOriginalExtension();
                $image->move(public_path('singers'),$fileName);
                File::delete(public_path('singers/'.$song->file));
                $song->name = $name;
                $song->album_id = $album_id;
                $song->price = $price;
                $song->file = $fileName;
                $song->save($songData);
            }
            return $song;
        }

        public function deleteSong($songId)
        {
            $song = Song::findOrFail($songId);
            if($song->file){
                File::delete(public_path('singers/'.$song->file));
            }
            $song->delete();
        }

    }
